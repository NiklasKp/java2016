# README #

Da wir mit dem Üben von Java für den Anfang mehr oder weniger auf uns alleine gestellt sind gibt es dieses Repository.

Jeder der/die eine interessante Übungsaufgabe gefunden hat (egal ob aus dem Internet, von Kahlbrandt oder irgend einem anderen Prof), kann die Aufgabenstellung und die mitgelieferten Dateien unter **problems/'aufgaben_name'/** hier hochladen.

Jeder der/die eine Lösung bzw einen Ansatz zu einer der Aufgaben hat, kann die unter dem Package 
**solutions.'aufgaben_name'.'name'** hochladen.

Mit der Zeit sollten sich so einige Übungsaufgaben (unter src/problems) mit unterschiedlichen Beispiellösungen (unter src/solutions) ansammeln.
Zusätzlich mit der Möglichkeit über die Commit-Kommentarfunktion von BitBucket Fragen zu den Lösungen Anderer zu stellen.

Idee wäre das Ganze auf Englisch zu halten, auch das ist als Übung sicher nicht verkehrt. Aber jeder wie er/sie will ;)

**Helpful Resources** for learning Java (stal):

* [Introduction to Computer Science using Java](http://programmedlessons.org/java5/index.html), a very extensive tutorial based on short pages with review questions, starting from first principles and going into details including GUIs and file I/O, with quizzes and exercises. A bit long in the tooth though, but Java *is* a language with some history, after all ... and most of the basics are still relevant. In any case it might be a good idea to just work through the quizzes so to find out where it might be worthwhile reading the chapter. 

* [Official Java Tutorials](http://docs.oracle.com/javase/tutorial/), including the latest changes in Java 8 (functional programming elements, streams, lambdas etc.).

* [Objects First With Java, by David Barnes and Michael Kölling, 5th ed.](http://ebook-dl.com/downloadebook/?objects-first-with-java-a-practical-introduction-using-bluej-david-j-barnes), free download of the Ebook version of this top/down teaching classic using the BlueJ IDE. Starts with object relations instead of language basic. Full of exercises which you may find familiar from the Ruby adaptation by Birgit W.

* [Thinking in Java, by Bruce Eckels, 3rd ed.](http://www.mindview.net/Books/TIJ/), also a free Ebook of the last-but-one edition of this highly praised classic. I hate it, but many people think different. :-) Anyway it's free.

* [Exercism](http://exercism.io/) has a Java track, of course.

**Hauptunterschiede zwischen Ruby und Java**
(Nur als Hinweis auf die ersten Hürden, über die man stolpern könnte ... Bitte gern weiter ergänzen!) 

* Java ist eine statisch typisierte Sprache: Jede Variable braucht eine Typdeklaration (int, double, boolean, String, YourClassName usw.), jede Methode die Erklärung des Rückgabewerts. Methoden, die nichts zurückgeben, sind "void". 
* Der Rückgabewert einer Methode muss immer mit "return" gekennzeichnet werden. Java-Methoden geben nichts automatisch zurück.
* Auch die Sichtbarkeit von Methoden und Variablen wird üblicherweise immer erklärt; die Keywords sind dieselben. Alles was nicht deklariert ist, ist von außen sichtbar (innerhalb des Pakets, jedenfalls). 
* Klassenmethoden und -variablen sind "static", Konstanten sind "final".  
* Die Klammern, die bei Ruby fast immer optional sind, sind in Java verpflichtend, insbesondere bei Bedingungen (if, while usw.), aber auch bei leeren Parameterlisten, sonst meckert direkt der Compiler. Jedes Statement endet mit Semikolon. Bedingungen enden *nicht* mit Semikolon (sonst habt Ihr einen leeren Body für die Bedingung, eine Quelle wunderbar schwer zu findender Bugs, da syntaktisch korrekt!).  
* Es gibt keine Skriptdateien. Was in Ruby ein Skript wäre, muss in Java stets in die Main-Methode einer Klasse! 
* Statt "initialize" haben Java-Klassen einen Constructor, der den gleichen Namen wie die Klasse hat. Oder mehrere: In Java kann man Methoden, und eben auch Constructors, "überladen", d.h. weitere Versionen mit abweichenden Parameterlisten zur Verfügung stellen. 
* Java ist eine kompilierte Sprache (Ruby eine interpretierte). Java-Sourcedateien (.java) können daher nicht direkt ausgeführt werden; sie werden kompiliert zu ausführbaren (aber dafür nicht menschenlesbaren) Bytedateien (.class). Davon merkt Ihr nichts, wenn Ihr in der IDE arbeitet, aber wissen sollte man es schon, denn es erklärt, warum der Compiler sich häufig über Dinge beschwert, die in Ruby erst zur Laufzeit aufgefallen wären. 
* Gegenüber Rubys universellen Containern Array und Hash hat Java eine Vielzahl von spezialisierten Sammlungstypen, die es anzuschauen lohnt, da sie in Verhalten und Performance sehr unterschiedlich sind. Im Gegenzug verfügt Java allerdings nicht über die Tonnen von spezialisierten Iteratoren, die für Ruby typisch sind; es dominiert das manuelle Iterieren per foreach ("for (Object o : list)") oder per externem Iterator. Die Stream API von Java 8 bringt allerdings die Möglichkeit, funktionalen Code mit Aneinanderreihung spezialisierter Methoden für Sammlungen zu schreiben, der dem Ruby-Stil sehr ähnlich sieht, aber ich würde vermuten, dass wir in PM2 eher die klassische Vorgehensweise lernen werden. 
* In Java ist *nicht* alles ein Objekt: Es gibt die primitiven Typen (int, double, char, boolean usw.), die nur zu Objekten werden (z.B. im Kontext von Sammlungen, die keine primitiven Typen enthalten können), wenn sie entsprechend verpackt werden (Boxing: Integer, Double, Character, Boolean usw.). 
* In Java, ziemlich wie in Ruby, werden primitive Typen beim Parameteraufruf direkt übergeben, von Objekten hingegen erhält man eine Referenz. Lustigerweise allerdings nennt man das in Ruby (wir haben das eingetrichtert bekommen) "call by reference", in Java hingegen ist es allgemein als "call by value" bekannt. Davon besser nicht verwirren lassen, es gibt praktisch keinen Unterschied. (Ich warte gespannt drauf, wie K. uns das erklären will! Hier etwas Lesestoff dazu: [Wikipedia > Evaluation Strategy > Call by Sharing](https://en.wikipedia.org/wiki/Evaluation_strategy#Call_by_sharing) (4. Satz beachten), [Is Ruby pass-by-reference or pass-by-value?](http://robertheaton.com/2014/07/22/is-ruby-pass-by-reference-or-pass-by-value/), [Java is Pass-by-value, Dammit!](http://javadude.com/articles/passbyvalue.htm). 
* Und böse Falle (würde K. vermutlich sagen): In Java testet == auf Objektidentität, .equals() (mit 's'!) auf Objektgleichheit. Also genau umgekehrt wie in Ruby! 
* Apropos .equals(), das ist natürlich entsprechend die Methode, die Ihr in Java überschreiben müsst, damit Eure Objekte gefunden und auf Gleichheit getestet werden können (zusammen mit hashCode()). Vielleicht hilfreicher Hinweis, der die Implementierung erleichtert: der Operator *instanceof*, der auf Typgleichheit prüft, enthält bereits einen Nulltest. Wenn Ihr also in einer Klasse myObject mit einem zu vergleichenden Feld myField (geistreich, ich weiß) sowas schreibt wie den nachstehenden Codeschnipsel, dann habt Ihr bereits einen Test, der false auch dann zurückgibt, wenn das andere Objekt null oder vom falschen Typ ist. Sehr einfach. 

```
#!java
public boolean equals(Object otherObject) { 
    return otherObject instanceof myObject && myField.equals(((myObject)otherObject).myField) 
}

```