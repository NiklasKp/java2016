package testing;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Utility class to collect Class-Objects from the .class files in the file
 * system.
 * 
 * @author Niklas Kopp
 */
public class Classes {
    
    private Classes() {
    }
    
    /**
     * Returns all classes that implement the assigned interface class as a
     * Collection of Object arrays (one class per array) to match parameterized
     * testing.
     * 
     * @param interfaceClass
     * @return
     */
    public static Iterable<Object[]> getTestParametersFor(Class<?> interfaceClass) {
        try {
            return getImplementing(interfaceClass)
                .stream()
                .map(clazz -> new Object[] {clazz})
                .collect(Collectors.toList());
                
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    /**
     * Returns all classes that implement the assigned interface class.
     * 
     * @param interfaceClass
     * @return
     * @throws IOException
     */
    public static Collection<Class<?>> getImplementing(Class<?> interfaceClass)
        throws IOException {
        return getAll()
            .stream()
            .filter(clazz -> Arrays.asList(clazz.getInterfaces()).contains(interfaceClass))
            .collect(Collectors.toList());
    }
    
    /**
     * Returns all classes found as .class Files under the root directories
     * 
     * @return
     * @throws IOException
     */
    public static Collection<Class<?>> getAll() throws IOException {
        List<Class<?>> classes = new ArrayList<>();
        
        ClassLoader loader = ClassLoader.getSystemClassLoader();
        if (loader == null) {
            throw new RuntimeException("Couldn't get class loader");
        }
        
        for (URL u : Collections.list(loader.getResources(""))) {
            addClassFiles(u, classes);
        }
        return classes;
    }
    
    // Adds all classes found at 'root' to the assigned Collection.
    private static void addClassFiles(URL root, Collection<Class<?>> classes) throws IOException {
        Path rootPath = null;
        try {
            rootPath = Paths.get(root.toURI());
        } catch (URISyntaxException e1) {
            throw new IOException("Error while loading the resource " + root);
        }
        
        for (Path classFile : getClassFiles(rootPath, new ArrayList<>())) {
            try {
                String fullClassName = rootPath.relativize(classFile)
                    .toString()
                    .replace(".class", "")
                    .replace(File.separatorChar, '.');
                    
                classes.add(Class.forName(fullClassName));
            } catch (ClassNotFoundException e) {
                // It's not there, so it's not added. Nothing to do here.
            }
        }
    }
    
    // Adds all files in the assigned directory (and all sub-directories) that
    // end with '.class' to the assigned Collection, then returns it.
    private static Collection<Path> getClassFiles(Path parentPath, Collection<Path> classFiles)
        throws IOException {
        for (Path p : Files.newDirectoryStream(parentPath)) {
            if (p.getFileName().toString().endsWith(".class")) {
                classFiles.add(p);
            } else if (Files.isDirectory(p)) {
                getClassFiles(p, classFiles);
            }
        }
        return classFiles;
    }
    
    /**
     * Returns an Optional containing a new instance of the assigned class using
     * the empty constructor or an empty Optional if an Exception is thrown
     * while doing so.
     * 
     * @param clazz
     * @return
     */
    @SuppressWarnings("unchecked")
    public static <T> Optional<T> getInstance(Class<?> clazz) {
        Optional<T> result;
        try {
            result = Optional.of((T) clazz.getConstructor().newInstance());
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
            | InvocationTargetException | NoSuchMethodException | SecurityException
            | ClassCastException e) {
            
            result = Optional.empty();
        }
        return result;
    }
}
