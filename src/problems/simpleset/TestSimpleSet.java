package problems.simpleset;

import static org.junit.Assert.*;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import org.junit.Before;
import org.junit.Test;

/**
 * Abstract Test for the SimpleSet interface. Extending Tests need to overwrite
 * the createSet() method.
 * 
 * @author Niklas Kopp
 */
public abstract class TestSimpleSet {
    private SimpleSet<String> set;
    
    /**
     * Needs to return a new empty instance of the SimpleSet implementation to
     * test.
     */
    public abstract <T> SimpleSet<T> createSet();
    
    @Before
    public void setUp() {
        set = createSet();
    }
    
    @Test(timeout = 1000)
    public void testAddRemoveSize() {
        assertEquals(0, set.size());
        assertTrue(set.isEmpty());
        
        set.add("One");
        assertEquals(1, set.size());
        assertFalse(set.isEmpty());
        
        set.add("Two");
        assertEquals(2, set.size());
        // Value already exists
        set.add("One");
        assertEquals(2, set.size());
        
        set.remove("Not included");
        assertEquals(2, set.size());
        
        set.remove("One");
        assertEquals(1, set.size());
        
        set.remove("Two");
        assertEquals(0, set.size());
        assertTrue(set.isEmpty());
        
        try {
            set.add(null);
        } catch (NullPointerException e) {} finally {
            assertFalse(set.contains(null));
        }
    }
    
    @Test(timeout = 1000)
    public void testContains() {
        set.add("One");
        set.add("Two");
        set.add("Three");
        
        assertTrue(set.contains("One"));
        assertTrue(set.contains("Two"));
        assertTrue(set.contains("Three"));
        
        set.remove("Two");
        assertFalse(set.contains("Two"));
        
        assertFalse(set.contains(null));
        assertFalse(set.contains("Not included"));
    }
    
    @Test(timeout = 1000)
    public void testArray() {
        set.add("Gone");
        
        set.add("One");
        set.add("Two");
        set.add("Three");
        
        set.remove("Gone");
        
        assertEquals(Arrays.asList("One", "Two", "Three"), Arrays.asList(set.toArray()));
        
        String[] s = new String[3];
        String[] s2 = set.toArray(s);
        
        assertEquals(Arrays.asList(s), Arrays.asList(s2));
    }
    
    @Test(timeout = 1000)
    public void testIterator() {
        List<String> values = Arrays.asList("1", "2", "2", "3", "3", "4");
        for (String str : values) {
            set.add(str);
        }
        
        Iterator<String> iterator = set.iterator();
        
        for (int i = 0; i < 4; i++) {
            assertTrue(iterator.hasNext());
            assertTrue(values.contains(iterator.next()));
        }
        assertFalse(iterator.hasNext());
    }
    
    @Test(timeout = 1000)
    public void testCapacity() {
        SimpleSet<Integer> intSet = createSet();
        
        for (int i = 0; i < 4200; i++) {
            intSet.add(i);
        }
        assertEquals(4200, intSet.size());
    }
}