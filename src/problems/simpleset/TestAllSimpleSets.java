package problems.simpleset;

import java.util.Optional;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import testing.Classes;

/**
 * Tests all implementations of 'problems.simpleset.Simpleset' that can be found
 * as .class files.
 * 
 * @author Niklas Kopp
 */
@RunWith(Parameterized.class)
public class TestAllSimpleSets extends TestSimpleSet {
    
    @Parameters(name = "{0}")
    public static Iterable<Object[]> getImplementingClasses() {
        return Classes.getTestParametersFor(SimpleSet.class);
    }
    
    @Parameter
    public Class<?> implementationClass; // injected by JUnit
    
    @Override
    public <T> SimpleSet<T> createSet() {
        Optional<SimpleSet<T>> result = Classes.getInstance(implementationClass);
        if (!result.isPresent()) {
            throw new RuntimeException("Couldn't create an instance of " + implementationClass);
        }
        return result.get();
    }
}
