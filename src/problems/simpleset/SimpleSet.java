package problems.simpleset;

/**
 * Ein Interface f�r Klassen, die eine "zu viele" Richtung einer bin�ren
 * Assoziation implementieren sollen. Die Hauptunterschiede zu
 * {@link jav.util.Set} sind:
 * <ul>
 * <li>Keine optionalen Operationen</li>
 * <li>Nur wenige Operationen</li>
 * <li>Das Set darf keine {@code null} Elemente enthalten</li>
 * </ul>
 * 
 * @author Bernd Kahlbrandt
 *         
 * @param <E>
 */
public interface SimpleSet<E> extends Iterable<E> {
  void add(E entry);
  
  void remove(E entry);
  
  boolean contains(E element);
  
  int size();
  
  boolean isEmpty();
  
  Object[] toArray();
  
  E[] toArray(E[] a);
}
