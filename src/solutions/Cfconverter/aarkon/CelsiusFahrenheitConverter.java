/**
 * 
 */
package solutions.Cfconverter.aarkon;

import java.util.Scanner;

/**
 * Trivial Converter for Celsius to Fahrenheit and backwards. Written mainly to
 * see my name here and calm myself because I did at least some programming
 * before exams. /o\ Also, these days I couldn't look over something dealing
 * with temperatures.
 * 
 * This approach takes the users's input in one to make the task a little more
 * challenging and repeat string deconstruction from the C-programming class.
 * 
 * @author Jakob Ledig
 *
 */
public class CelsiusFahrenheitConverter {

	public static void main(String[] args) {
		System.out
				.println("This program will convert Fahrenheit to Celsius and vice versa.");
		String input = interact();
		Double degrees = getNum(input);
		char unit = getUnit(input);
		Double output = convert(degrees, unit);
		System.out.println(input + " is equivalent to: \n " + output + "°"
				+ inverseUnit(unit));
	}

	private static char inverseUnit(char unit) {
		if ((unit == 'f') || (unit == 'F')) {
			unit = 'C';
		} else if ((unit == 'c') || (unit == 'C')) {
			unit = 'F';
		}
		return unit;
	}

	private static Double convert(Double degrees, char unit)
	/**
	 * @param Double
	 * @param char
	 * 
	 *        Does the math. IllegalArgumentException could be discarded 'cause
	 *        the method is private and the input is sanitised before, but hey.
	 *        This is practice after all. ¯\_(ツ)_/¯
	 */
	throws IllegalArgumentException {
		if ((unit == 'c') || (unit == 'C')) {
			degrees = (degrees * (9.0 / 5.0)) + 32;
		} else if ((unit == 'F') || (unit == 'f')) {
			degrees = (degrees - 32) * (5.0 / 9.0);
		} else {
			throw new IllegalArgumentException(
					"Didn't receive the right unit to compute");
		}
		return degrees;
	}

	private static char getUnit(String input) {
		/**
		 * @param String
		 *            input Returns the first occurrence of any of 'F', 'f', 'C'
		 *            or 'c' in the input string.
		 */
		char[] characters = input.toCharArray();
		char unit = '0';
		for (char c : characters) {
			if ((c == 'F') || (c == 'f') || (c == 'C') || (c == 'c')) {
				unit = c;
				break;
			}
		}
		return unit;
	}

	private static Double getNum(String input) {
		/**
		 * @param String
		 *            Returns all the decimal numbers in the input string. Stops
		 *            looking for numbers at the first char in the input string
		 *            containing anything else than a number.
		 */
		Double numbers = 0.0;
		char[] before, after;
		boolean negative = false;
		if (input.charAt(0) == '-'){
			negative = true;
			input = input.substring(1);
		}

		// split input at comma if present and if not set 'after' to null
		if (input.contains(",")) {
			before = input.substring(0, input.indexOf(",")).toCharArray();
			after = input.substring((input.indexOf(",") + 1),
					(input.length() - 1)).toCharArray();
		} else {
			before = input.toCharArray();
			after = null;
		}

		// get numbers from the chars in front of comma
		for (char c : before) {
			if ((c >= '0') && (c <= '9')) {
				numbers = (numbers * 10) + (c - 48); // retrieving c's numeric
														// value by substracting
														// it's position in the
														// ASCII-table
			} else {
				break;
			}
		}

		// get numbers from second part of string if present
		if (after != null) {
			int i = 0;
			for (char c : after) {
				if ((c >= '0') && (c <= '9')) {
					i++;
					numbers = numbers
							+ ((c - 48) * (Math.pow(10, (double) (i * -1))));
				} else {
					break;
				}
			}
		}
		if (negative){
			return numbers * -1;
		} else {
		return numbers;
		}
	}

	private static String interact() {
		Scanner reader = new Scanner(System.in);
		System.out
				.println("Enter a temperature you want converted (with the scale, e.g. '32,1C'! (use ',' instead of '.')");
		String output = reader.nextLine();
		reader.close();
		return output;
	}
}