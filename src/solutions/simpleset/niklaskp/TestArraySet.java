package solutions.simpleset.niklaskp;

import problems.simpleset.SimpleSet;
import problems.simpleset.TestSimpleSet;

/**
 * Test for the ArraySet class.
 * 
 * @author Niklas Kopp
 */
public class TestArraySet extends TestSimpleSet {  
    @Override
    public <T> SimpleSet<T> createSet() {
        return new ArraySet<>();
    }
}