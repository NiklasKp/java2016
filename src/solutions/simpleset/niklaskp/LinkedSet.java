package solutions.simpleset.niklaskp;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;
import problems.simpleset.SimpleSet;

/**
 * SimpleSet implementation based on a LinkedList structure.
 * 
 * @author Niklas Kopp
 * @param <E>
 *            the Type of elements stored in this Set.
 */
public class LinkedSet<E> implements SimpleSet<E> {
    
    private E value;
    private LinkedSet<E> next;
    
    public LinkedSet() {
        this(null);
    }
    
    private LinkedSet(E val) {
        value = val;
        next = null;
    }
    
    @Override
    public Iterator<E> iterator() {
        return new IteratorLS(this);
    }
    
    @Override
    public void add(E entry) {
        Objects.requireNonNull(entry);
        
        if (entry.equals(value)) {
            return;
        }
        
        if (next == null) {
            next = new LinkedSet<E>(entry);
        } else {
            next.add(entry);
        }
    }
    
    @Override
    public void remove(E entry) {
        if (entry != null && next != null) {
            next.remove(entry, this);
        }
    }
    
    private void remove(E entry, LinkedSet<E> before) {
        if (entry.equals(value)) {
            before.next = this.next;
        } else if (next != null) {
            next.remove(entry, this);
        }
    }
    
    @Override
    public boolean contains(E element) {
        if (element == null) {
            return false;
        }
        for (E other : this) {
            if (element.equals(other)) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    @SuppressWarnings("unused")
    public int size() {
        int count = 0;
        for (E element : this) {
            count++;
        }  
        return count;
    }
    
    @Override
    public boolean isEmpty() {
        return next == null;
    }
    
    @Override
    public Object[] toArray() {
        Object[] resultArray = new Object[this.size()];
        int count = 0;
        for (E element : this) {
            resultArray[count] = element;
            count++;
        }
        return resultArray;
    }
    
    @Override
    public E[] toArray(E[] a) {
        int count = 0;
        
        for (E element : this) {
            a[count] = element;
            count++;
        }
        return a;
    }
    
    private class IteratorLS implements Iterator<E> {
        private LinkedSet<E> position;
        
        public IteratorLS(LinkedSet<E> start) {
            position = start;
        }
        
        @Override
        public boolean hasNext() {
            return position.next != null;
        }
        
        @Override
        public E next() {
            position = position.next;
            if(position == null){
                throw new NoSuchElementException();
            }
            return position.value;
        }
    }
}
