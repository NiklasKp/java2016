package solutions.simpleset.niklaskp;

import problems.simpleset.SimpleSet;
import problems.simpleset.TestSimpleSet;

/**
 * Test for the LinkedSet class.
 * 
 * @author Niklas Kopp
 */
public class TestLinkedSet extends TestSimpleSet {
    @Override
    public <T> SimpleSet<T> createSet() {
        return new LinkedSet<>();
    }
}
