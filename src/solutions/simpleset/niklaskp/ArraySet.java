package solutions.simpleset.niklaskp;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;
import problems.simpleset.SimpleSet;

/**
 * SimpleSet implementation based on an Array.
 * 
 * @author Niklas Kopp
 * @param <E>
 *            the Type of elements stored in this Set.
 */
public class ArraySet<E> implements SimpleSet<E> {
    
    private int capacity;
    private int nextCapacity;
    private E[] elements;
    private int size;
    
    public ArraySet() {
        size = 0;
        capacity = 0;
        nextCapacity = 10;
        ensureCapacity();
    }
    
    @SuppressWarnings("unchecked")
    private void ensureCapacity() {
        if (capacity > size()) {
            return;
        }
        capacity = nextCapacity;
        nextCapacity = (capacity / 2) * 3;
        
        E[] temp = elements;
        elements = (E[]) new Object[capacity];
        
       for(int i = 0; i < size(); i++){
           elements[i] = temp[i];
       }
    }
    
    @Override
    public Iterator<E> iterator() {
        return new IteratorAS(this);
    }
    
    @Override
    public void add(E entry) {
        Objects.requireNonNull(entry);
        if (!this.contains(entry)) {
            ensureCapacity();
            elements[size] = entry;
            size++;
        }
    }
    
    @Override
    public void remove(E entry) {
        if (entry == null) {
            return;
        }
        for (int i = 0; i < size(); i++) {
            if (entry.equals(elements[i])) {
                // Fill the gap
                for (int j = i + 1; j < size(); j++) {
                    elements[j - 1] = elements[j];
                }
                size--;
                return;
            }
        }
    }
    
    @Override
    public boolean contains(E element) {
        if (element == null) {
            return false;
        }
        for (E other : this) {
            if (element.equals(other)) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public int size() {
        return size;
    }
    
    @Override
    public boolean isEmpty() {
        return size() == 0;
    }
    
    @Override
    public Object[] toArray() {
        Object[] result = new Object[size()];
        for (int i = 0; i < size(); i++) {
            result[i] = elements[i];
        }
        return result;
    }
    
    @Override
    public E[] toArray(E[] a) {
        for (int i = 0; i < size(); i++) {
            a[i] = elements[i];
        }
        return a;
    }
    
    private class IteratorAS implements Iterator<E> {
        
        private ArraySet<E> set;
        private int position;
        
        public IteratorAS(ArraySet<E> set) {
            this.set = set;
            position = -1;
        }
        
        @Override
        public boolean hasNext() {
            return position + 1 < set.size();
        }
        
        @Override
        public E next() {
            position++;
            if (position >= set.size()) {
                throw new NoSuchElementException();
            }
            return set.elements[position];
        }
    }
}
