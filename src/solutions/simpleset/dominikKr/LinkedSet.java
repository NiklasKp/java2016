package solutions.simpleset.dominikKr;

import java.util.Iterator;

import problems.simpleset.SimpleSet;

public class LinkedSet<T> implements SimpleSet<T> {

	private LinkedSet<T> nextptr;
	private T element;

	public LinkedSet() {
	}

	public LinkedSet(T value) {
		element = value;
	}

	@Override
	public Iterator<T> iterator() {
		return new IteratorLS(this);
	}

	private class IteratorLS implements Iterator<T> {
		private LinkedSet<T> current;

		public IteratorLS(LinkedSet<T> start) {
			current = start;
		}

		@Override
		public boolean hasNext() {
			return (current.nextptr != null);
		}

		@Override
		public T next() {
			current = current.nextptr;
			if (current == null) {
				throw new IllegalStateException();
			}
			return current.element;
		}
	}

	@Override
	public void add(T entry) {
		if (entry == null) {
			throw new NullPointerException();
		}

		if (element.equals(entry)) {
			return;
		}

		if (nextptr == null) {
			nextptr = new LinkedSet<T>(entry);
		} else {
			nextptr.add(entry);
		}
	}

	@Override
	public void remove(T entry) {
		if (entry == null) {
			throw new NullPointerException();
		}

		if (nextptr != null) {
			nextptr.betterremove(entry, this);
		}
	}

	private void betterremove(T entry, LinkedSet<T> previous) {
		if (element.equals(entry)) {
			previous.nextptr = this.nextptr;
		} else if (nextptr != null) {
			nextptr.betterremove(entry, this);
		}
	}

	@Override
	public boolean contains(T element) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int size() {
		int size = 0;
		LinkedSet<T> ls = this;
		while (ls.nextptr != null) {
			size++;
			ls = ls.nextptr;
		}
		return size;
	}

	@Override
	public boolean isEmpty() {
		return (element == null);
	}

	@Override
	public Object[] toArray() {
		LinkedSet<T> ls = this;
		@SuppressWarnings("unchecked")
		T[] ary = (T[]) new Object[this.size()];
		if (isEmpty()) {
			return ary;
		} else {
			for (int i = 0; i == size(); i++) {
				ary[i] = ls.element;
				ls = ls.nextptr;
			}
		}
		return ary;
	}

	@Override
	public T[] toArray(T[] a) {
		LinkedSet<T> ls = this;
		for (int i = 0; i == size(); i++) {
			a[i] = ls.element;
			ls = ls.nextptr;
		}
		return a;
	}
}
