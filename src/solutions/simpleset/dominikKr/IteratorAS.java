package solutions.simpleset.dominikKr;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class IteratorAS<T> implements Iterator<T> {
	private int i;
	private ArraySet<T> as;
	
	public IteratorAS(ArraySet<T> as){
		this.as = as;
		i = -1;
	}

	@Override
	public boolean hasNext() {
		return (i+1 < as.size());
	}

	@SuppressWarnings("unchecked")
	@Override
	public T next() {
		i++;
		if (i > as.size()-1){
			throw new NoSuchElementException();
		}
		return (T) as.toArray()[i];
	}

}