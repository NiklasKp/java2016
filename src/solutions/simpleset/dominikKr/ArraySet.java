package solutions.simpleset.dominikKr;

import java.util.Iterator;

import problems.simpleset.SimpleSet;

public class ArraySet<T> implements SimpleSet<T> {
	private T set[];
	private int ptr;
	private int cap;

	@SuppressWarnings("unchecked")
	public ArraySet() {
		cap = 20;
		set = (T[]) new Object[cap];
		ptr = 0;
	}

	@Override
	public Iterator<T> iterator() {
		return new IteratorAS<T>(this);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void add(T entry) {
		if (entry == null) {
			throw new NullPointerException();
		}
		if (ptr == cap) {
			cap += cap;
			T[] temp = set;
			set = (T[]) new Object[cap];
			for (int i = 0 ; i < size(); i++) {
				set[i] = temp[i];
			}
		}
		if (!contains(entry)) {
			set[ptr] = entry;
			ptr++;
		}
	}

	@Override
	public void remove(T entry) {
		if (!isEmpty()) {
			boolean flag = false;
			if (contains(entry)) {
				for (int i = 0; i < size(); i++) {
					if (flag) {
						set[i - 1] = set[i];
					}
					if (set[i].equals(entry)) {
						set[i] = null;
						flag = true;
					}
				}
				ptr--;
			}
		}
	}

	@Override
	public boolean contains(T element) {
		for (T ele : this) {
			if (ele.equals(element)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public int size() {
		return ptr;
	}

	@Override
	public boolean isEmpty() {
		return (size() == 0);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object[] toArray() {
		return toArray((T[]) new Object[size()]);
	}

	@Override
	public T[] toArray(T[] a) {
		for (int i = 0; i < size(); i++){
			a[i] = set[i];
		}
		return a;
	}
}
