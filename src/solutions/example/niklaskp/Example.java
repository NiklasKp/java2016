// Every solution's package would be named:
// solutions.<name of the problem package>.<your name>
package solutions.example.niklaskp;

// Example import of resources from the corresponding problems package.
import problems.example.RubyOutput;

/**
 * A super complicated example class.
 * 
 * @author NiklasKp
 */
public class Example {
  public static void main(String... args) {
    RubyOutput.puts("Hello World");
  }
}
