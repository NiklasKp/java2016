package solutions.Grains.jagoenau;

public class Grains {
	private static int intTotalRice;
	private static long longTotalRice;
	private static long rice;
	
	private static void calculateRice(int square){
		 rice = (long) Math.pow(2,(square-1));
	}
	
	private static void calculateTotalRice(){
		intTotalRice+= rice;
		longTotalRice += rice;
	}
	
	public static void main(String... args){
		for (int i=1; i<=64; i++){
			calculateRice(i);
			calculateTotalRice();
			System.out.println("Square: " + i);
			System.out.println("Rice: " + rice);
			System.out.println("total Rice (int): " + intTotalRice);
			System.out.println("total Rice (long): " + longTotalRice);
			System.out.println("----------------------------");
			
		}
	}

}
