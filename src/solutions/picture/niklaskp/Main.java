package solutions.picture.niklaskp;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * Main class for the steganographic exercise.
 * 
 * @author Niklas Kopp
 */
public class Main {
    public static void main(String... strings) {
        try {
            PictureReader reader = new PictureReader(choosePng());
            System.out.println("Extracted file " + reader.extractFile(choosePath()));
            
        } catch (Exception e) {
            System.out.println("Unable to extract hidden data from file");
            System.out.println(e.getClass() + " " + e.getMessage());
        }
    }
    
    private static String choosePng() {
        String result = null;
        JFileChooser chooser = new JFileChooser();
        
        chooser.setDialogTitle("Select PNG File");
        chooser.setFileFilter(new FileNameExtensionFilter("PNG", "PNG"));
        chooser.setAcceptAllFileFilterUsed(false);
        if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
            result = chooser.getSelectedFile().toString();
        } else {
            exit();
        }
        return result;
    }
    
    private static String choosePath() {
        String result = null;
        JFileChooser chooser = new JFileChooser();
        
        chooser.setDialogTitle("Select directory for extracted file");
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setAcceptAllFileFilterUsed(false);
        if (chooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
            result = chooser.getSelectedFile().toString();
        } else {
            exit();
        }
        return result;
    }
    
    private static void exit() {
        System.out.println("Aborted by user");
        System.exit(0);
    }
}
