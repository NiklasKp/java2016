package solutions.picture.niklaskp;

import java.util.function.Predicate;

/**
 * Utility class with methods for parameter validation. Based on
 * com.google.common.base.Preconditions but smaller and stand-alone.
 * 
 * @author Niklas Kopp
 */
public class Preconditions {
    
    private Preconditions() {}
    
    /**
     * Throws a NullpointerException if the assigned reference is null,
     * otherwise returns the reference.
     */
    public static <T> T checkNotNull(T reference) {
        return checkNotNull(reference, null);
    }
    
    /**
     * Throws a NullpointerException with the assigned errorMessage if the
     * assigned reference is null, otherwise returns the reference.
     */
    public static <T> T checkNotNull(T reference, String errorMessage) {
        if (reference == null) {
            throw new NullPointerException(firstNonNull(errorMessage, ""));
        }
        return reference;
    }
    
    /**
     * Returns the first reference that is not null. Throws a
     * NullPointerExcetion if both are null.
     */
    public static <T> T firstNonNull(T first, T second) {
        return (first == null) ? checkNotNull(second) : first;
    }
    
    /**
     * Throws an IllegalArgumentException with if the assigned expression is
     * false.
     */
    public static void checkArgument(boolean expression) {
        checkArgument(expression, null);
    }
    
    /**
     * Throws an IllegalArgumentException with the assigned errorMessage if the
     * assigned expression is false.
     */
    public static void checkArgument(boolean expression, String errorText) {
        if (!expression) {
            throw new IllegalArgumentException(firstNonNull(errorText, ""));
        }
    }
    
    /**
     * Throws an IllegalArgumentException if the assigned Predicate evaluates to
     * false.
     */
    public static <T> T checkArgument(T argument, Predicate<T> condition) {
        return checkArgument(argument, condition, null);
    }
    
    /**
     * Throws an IllegalArgumentException with the assigned errorMessage if the
     * assigned Predicate evaluates to false.
     */
    public static <T> T checkArgument(T argument, Predicate<T> condition, String errorText) {
        checkArgument(condition.test(argument),
            firstNonNull(errorText, "Illegal Argument " + argument));
            
        return argument;
    }
    
    /**
     * Throws an IllegalStateException with if the assigned expression is false.
     */
    public static void checkState(boolean expression) {
        checkArgument(expression, null);
    }
    
    /**
     * Throws an IllegalStateException with the assigned errorMessage if the
     * assigned expression is false.
     */
    public static void checkState(boolean expression, String errorText) {
        if (!expression) {
            throw new IllegalStateException(firstNonNull(errorText, ""));
        }
    }
    
    /**
     * Throws an IndexOutOfBoundsException unless 0 <= index < size.
     */
    public static int checkIndex(int index, int size) {
        return checkIndex(index, size, null);
    }
    
    /**
     * Throws an IndexOutOfBoundsException with the assigned errorMessage unless
     * 0 <= index < size.
     */
    public static int checkIndex(int index, int size, String errorMessage) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException(
                firstNonNull(errorMessage, "Index out of bounds: " + index));
        }
        return index;
    }
}
