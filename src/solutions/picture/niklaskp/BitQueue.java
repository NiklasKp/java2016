package solutions.picture.niklaskp;

import static solutions.picture.niklaskp.Preconditions.*;

/**
 * Queue to store bits independently from the byte-structure. Maximum capacity
 * is 64 bit.
 * 
 * @author Niklas Kopp
 */
public class BitQueue {
    private static final String ERROR_NEGATIVE = "Number of bits needs to be positive.";
    private static final int CAPACITY = 64;
    
    private long buffer = 0L;
    private int bufferSize = 0;
    private int[] masks = new int[33];
    
    /**
     * Creates an empty BitQueue.
     */
    public BitQueue() {
        // Creating all bit masks
        for (int index = 1; index <= 32; index++) {
            masks[index] = (masks[index - 1] << 1) | 1;
        }
    }
    
    /**
     * Removes and returns the assigned number of bits. First in, first out.
     * 
     * @param bits
     *            the number of bits to remove
     * @return An int with the requested bits stored in the LSBits.
     */
    public int take(int bits) {
        checkArgument(bits > 0, ERROR_NEGATIVE);
        checkArgument(bits <= bufferSize, "Not enough bits in the buffer");
        
        int offset = bufferSize - bits;
        int result = (int) buffer >> offset;
        bufferSize -= bits;
        return result & masks[bits];
    }
    
    /**
     * Adds the requested number of bits from the value parameter. First in,
     * first out.
     * 
     * @param value
     *            an int where the LSBits store the information to be added.
     * @param bits
     *            the number of bits to be read from the value parameter.
     */
    public void add(int value, int bits) {
        checkArgument(bits > 0, ERROR_NEGATIVE);
        checkArgument(bits + bufferSize <= CAPACITY, "BitQueue is out of capacity");
        
        buffer = (buffer << bits) | (value & masks[bits]);
        bufferSize += bits;
    }
    
    /**
     * Returns the size of this BitQueue.
     * 
     * @return The number of bits stored in this BitQueue.
     */
    public int size() {
        return bufferSize;
    }
}
