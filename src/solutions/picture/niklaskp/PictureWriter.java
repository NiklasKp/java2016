package solutions.picture.niklaskp;

import static solutions.picture.niklaskp.Preconditions.*;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.imageio.ImageIO;

/**
 * Picturewriter can be used to write a file into an uncompressed image file
 * using the two lowest bits of each color channel.
 * 
 * @author Niklas Kopp
 */
public class PictureWriter {
    private static final int BIT_MASK_TWO = ~0b11; // ...1111100
    private static final int BITS_USED = 2;
    
    private final BufferedImage image;
    private final File destination;
    private IteratorXY coord;
    private BitQueue bits;
    private boolean closed = false;
    
    /**
     * Creates a new PictureWriter reading from an uncompressed image file and
     * writing an altered version of it when closed.
     * 
     * @param sourcePath
     *            the Image to read from
     * @param destinationPath
     *            the image path to write the altered image to
     * @throws IOException
     */
    public PictureWriter(String sourcePath, String destinationPath) throws IOException {
        // Source path is checked by ImageIO.
        checkArgument(Paths.get(destinationPath).getParent(), Files::exists,
            "Destination path doesn't exist: " + destinationPath);
            
        this.image = ImageIO.read(new File(checkNotNull(sourcePath)));
        this.destination = new File(destinationPath);
        coord = new IteratorXY(image.getWidth(), image.getHeight());
        bits = new BitQueue();
    }
    
    /**
     * Hides the file at the assigned path into the image.
     * 
     * @param filePath
     *            path of the file to hide
     * @throws IOException
     */
    public void write(String filePath) throws IOException {
        Path path = checkArgument(Paths.get(filePath), Files::exists,
            "File doesn't exist " + filePath);
            
        // Write size, splitting the size value to four separate bytes
        int size = (int) Files.size(path);
        for (int i = 3; i >= 0; i--) {
            write((byte) (size >>> (i * 8)));
        }
        // Write name
        for (byte b : path.getFileName().toString().getBytes()) {
            write(b);
        }
        // followed by a separator
        write((byte) 0);
        
        // Write data
        for (byte b : Files.readAllBytes(path)) {
            write(b);
        }
        close();
    }
    
    /**
     * Writes a byte of data into the image.
     * 
     * @param toWrite
     */
    public void write(byte toWrite) {
        bits.add(toWrite, 8);
        writeFromBuffer();
    }
    
    // flushes the buffer to the output image while there are at least 6 bits in
    // it.
    private void writeFromBuffer() {
        while (bits.size() >= BITS_USED * 3) {
            if (!coord.hasNext()) {
                throw new RuntimeException("Unexpected end of file.");
            }
            coord.next();
            Color oldColor = new Color(image.getRGB(coord.getX(), coord.getY()));
            
            int red = (oldColor.getRed() & BIT_MASK_TWO) | bits.take(BITS_USED);
            int green = (oldColor.getGreen() & BIT_MASK_TWO) | bits.take(BITS_USED);
            int blue = (oldColor.getBlue() & BIT_MASK_TWO) | bits.take(BITS_USED);
            
            image.setRGB(coord.getX(), coord.getY(), new Color(red, green, blue).getRGB());
        }
    }
    
    /**
     * Writes the remaining bits from the buffer and the altered image to the
     * disk
     * 
     * @throws IOException
     */
    public void close() throws IOException {
        if (closed) {
            return;
        }
        if(bits.size() < BITS_USED * 3){
            bits.add(0, BITS_USED * 3);
        }
        writeFromBuffer();
        
        ImageIO.write(image, "PNG", destination);
        closed = true;
    }
}
