package solutions.picture.niklaskp;

import static solutions.picture.niklaskp.Preconditions.*;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import javax.imageio.ImageIO;

/**
 * Class to read hidden information (the lowest two bits of each color channel
 * of each pixel) out of an uncompressed image file.
 * 
 * @author Niklas Kopp
 */
public class PictureReader {
    
    private final BufferedImage image;
    private Iterator<Color> pixelIterator;
    private BitQueue bits;
    
    /**
     * Creates a new reader that extracts a hidden file from an image.
     * 
     * @param path
     *            the path of the image file
     * @throws IOException
     *             when loading the image fails
     */
    public PictureReader(String path) throws IOException {
        image = ImageIO.read(new File(checkNotNull(path)));
        pixelIterator = new IteratorXY(image.getWidth(), image.getHeight())
            .convert((x, y) -> new Color(image.getRGB(x, y)));
        bits = new BitQueue();
    }
    
    /**
     * Extracts a hidden file to the assigned destination path.
     * 
     * @param destinationPath
     *            the path to extract the file to
     * @return the name of the extracted file
     * @throws IOException
     */
    public String extractFile(String destinationPath) throws IOException {
        checkArgument(Paths.get(destinationPath), Files::exists,
            "Destination path doesn't exist " + destinationPath);
        // Extracting the file size
        int size = 0;
        for (int i = 0; i < 4; i++) {
            size = (size << 8) | read();
        }
        
        // Extracting the file name
        String fileName = "";
        int character;
        while (true) {
            character = read();
            if (character == 0) {
                break;
            }
            fileName += (char) character;
        }
        
        // Extracting the data
        byte[] data = new byte[size];
        for (int i = 0; i < size; i++) {
            data[i] = (byte) read();
        }
        
        Files.write(Paths.get(destinationPath + File.separator + fileName), data);
        return fileName;
    }
    
    /**
     * Reads the next byte from the image, reading the lowest two bits of each
     * color of each pixel, starting in the upper left corner.
     * 
     * @return the next byte
     */
    public int read() {
        Color rgb;
        // Fill the buffer until there are at least 8 bits in it.
        while (bits.size() < 8) {
            if (!pixelIterator.hasNext()) {
                throw new RuntimeException("Unexpected end of file.");
            }
            rgb = pixelIterator.next();
            
            bits.add(rgb.getRed(), 2);
            bits.add(rgb.getGreen(), 2);
            bits.add(rgb.getBlue(), 2);
        }
        return bits.take(8);
    }
}
