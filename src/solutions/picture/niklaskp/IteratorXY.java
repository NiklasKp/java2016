package solutions.picture.niklaskp;

import java.awt.Point;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * Iterator class to iterate over two-dimensional indices.
 * 
 * @author Niklas Kopp
 */
public class IteratorXY implements Iterator<Point>, Iterable<Point> {
    
    private int x;
    private int y;
    private final int xSize;
    private final int ySize;
    
    /**
     * Creates a new Iterator, starting at (-1, 0) up to (xSize -1, ySize-1)
     */
    public IteratorXY(int xSize, int ySize) {
        this(-1, 0, xSize, ySize);
    }
    
    /**
     * Creates a new Iterator, starting at (xStart, yStart) up to (xSize -1,
     * ySize-1)
     */
    public IteratorXY(int xStart, int yStart, int xSize, int ySize) {
        x = xStart;
        y = yStart;
        this.xSize = xSize;
        this.ySize = ySize;
    }
    
    /**
     * Converts this Iterator to an Iterator of the type T using the assigned
     * conversion function to convert each value obtained by next().
     */
    public <T> Iterator<T> convert(Function<Point, T> conversion) {
        return new FunctionIterator<T>(this, conversion);
    }
    
    /**
     * Converts this Iterator to an Iterator of the type T using the assigned
     * conversion function to convert each value obtained by next().
     */
    public <T> Iterator<T> convert(BiFunction<Integer, Integer, T> conversion) {
        return new FunctionIterator<T>(this, (Point p) -> conversion.apply(p.x, p.y));
    }
    
    /**
     * Returns the current x value
     */
    public int getX() {
        return x;
    }
    
    /**
     * Returns the current y value
     */
    public int getY() {
        return y;
    }

    @Override
    public boolean hasNext() {
        return x + 1 < xSize || y + 1 < ySize;
    }
    
    @Override
    public Point next() {
        x++;
        if (x >= xSize) {
            x = 0;
            y++;
        }
        if (y >= ySize) {
            throw new NoSuchElementException();
        }
        return new Point(x, y);
    }
    
   
    @Override
    public Iterator<Point> iterator() {
        return this;
    }
    
    // Mounts a conversion function to an Iterator
    private class FunctionIterator<T> implements Iterator<T> {
        
        private Iterator<Point> parent;
        private Function<Point, T> conversion;
        
        public FunctionIterator(IteratorXY parent, Function<Point, T> conversion) {
            this.parent = parent;
            this.conversion = conversion;
        }
        
        @Override
        public boolean hasNext() {
            return parent.hasNext();
        }
        
        @Override
        public T next() {
            return conversion.apply(parent.next());
        }
    }
}
