package solutions.picture.stal;

import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Iterator;
import javax.imageio.ImageIO;

/**
 * Hides a file in the least significant bits of the color components of an
 * uncompressed image.
 * 
 * @author Steffen Albrecht
 */

public class FileEncoder {
    
    String workingDirectory;
    String sourceFileName;
    String targetFileName;
    String outputFileName;
    BufferedImage target;
    Iterator<Point> iterator;
    
    /**
     * Constructor
     * 
     * @param workingDirectory
     *            The directory holding the files to be encoded. The output file
     *            will also be placed in this directory.
     * @param sourceFileName
     *            The file that is to be hidden.
     * @param targetFileName
     *            The image the hidden file will be hidden in.
     * @throws IOException
     */
    public FileEncoder(String workingDirectory, String sourceFileName, String targetFileName,
        String outputFileName) throws IOException {
        this.workingDirectory = workingDirectory;
        this.sourceFileName = sourceFileName;
        this.targetFileName = targetFileName;
        this.outputFileName = outputFileName;
        File targetFile = new File(workingDirectory + targetFileName);
        target = ImageIO.read(targetFile);
        iterator = new CoordinateIterator(target);
    }
    
    // Compose the information to be hidden in the target image, split it into
    // bytes each holding two bits in its LSBits, write these bits into the
    // LSBits of successive color components of the hiding image, and export
    // that image to a png file.
    public void encodeFile() throws IOException {
        ByteBuffer bytesToHide = splitBytes(composeHiddenFile());
        writeImage(target, bytesToHide);
        File outputFile = new File(workingDirectory + outputFileName + ".png");
        ImageIO.write(target, "png", outputFile);
    }
    
    // Write the length of the hidden file in bytes, the file name as a char
    // sequence, a 0 byte as delimiter, and the file itself to a ByteBuffer.
    private ByteBuffer composeHiddenFile() throws IOException {
        // FileInputStream source = new FileInputStream(workingDirectory +
        // sourceFileName);
        RandomAccessFile source = new RandomAccessFile(workingDirectory + sourceFileName, "r");
        FileChannel channel = source.getChannel();
        int sourceLength = (int) channel.size();
        int totalSourceBytes = Constants.INTEGER_SIZE_IN_BYTES + sourceFileName.length() + 1
            + sourceLength;
        int numberOfBitPairs = totalSourceBytes * Constants.BIT_PAIRS_PER_BYTE;
        int colorComponentsInImage = target.getWidth() * target.getHeight()
            * Constants.COLOR_COMPONENTS_PER_PIXEL;
        if (numberOfBitPairs > colorComponentsInImage) {
            source.close();
            throw new IllegalArgumentException(
                "Source file is too large to be hidden in target image");
        }
        ByteBuffer buffer = ByteBuffer.allocate(totalSourceBytes);
        // Write the file length.
        buffer.putInt(sourceLength);
        // Write the file name.
        sourceFileName.chars().mapToObj(c -> (byte) c).forEachOrdered(ch -> buffer.put(ch));
        // Write the delimiter.
        buffer.put((byte) 0);
        // Write the file.
        channel.read(buffer);
        source.close();
        channel.close();
        return buffer;
    }
    
    // Split one byte into four pairs of bytes and write them each to a new
    // empty byte as LSBits.
    private ByteBuffer splitBytes(ByteBuffer in) {
        in.flip();
        ByteBuffer out = ByteBuffer.allocate(in.limit() * 4);
        while (in.hasRemaining()) {
            byte b = in.get();
            out.put((byte) (b >> 6 & Constants.LAST_TWO));
            out.put((byte) (b >> 4 & Constants.LAST_TWO));
            out.put((byte) (b >> 2 & Constants.LAST_TWO));
            out.put((byte) (b & Constants.LAST_TWO));
        }
        return out;
    }
    
    // Write the one-bit-pair bytes of the entire hidden file to the hiding image.
    private void writeImage(BufferedImage image, ByteBuffer bytes) {
        bytes.flip();
        while (iterator.hasNext() && bytes.hasRemaining()) {
            writeLSBits(iterator.next(), bytes);
        }
    }
    
    // Overwrite the LSBits of the color components of a single pixel with the
    // LSBit pairs from up to three one-bit-pair bytes.
    private void writeLSBits(Point position, ByteBuffer source) {
        int old = target.getRGB(position.x, position.y);
        int encoded = old;
        if (source.hasRemaining()) {
            encoded = (encoded & Constants.RED_FIRST_SIX) | (source.get() << 16);
        }
        if (source.hasRemaining()) {
            encoded = (encoded & Constants.GREEN_FIRST_SIX) | (source.get() << 8);
        }
        if (source.hasRemaining()) {
            encoded = (encoded & Constants.BLUE_FIRST_SIX) | source.get();
        }
        target.setRGB(position.x, position.y, encoded);
    }
}
