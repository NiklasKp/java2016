package solutions.picture.stal;

import java.io.IOException;
import java.util.Scanner;

/**
 * Wraps a FileDecoder and FileEncoder with user dialogues. 
 * 
 * @author Steffen Albrecht
 */
public class Main {
    
    static Scanner scan = new Scanner(System.in);
    
    public static void main(String[] args) {
        
        System.out.println("Decode (d) or encode (e)?");
        String choice = scan.nextLine();
        switch (choice) {
        case "d":
            decode();
            break;
        case "e":
            encode();
            break;
        default:
            System.out.println("Not a valid choice.");
        }
    }
    
    private static void decode() {
        String workingDirectory = UserInteraction.getDirectoryPath("working directory",
            System.getProperty("user.home"), true);
        String inputFileName = UserInteraction.getFileName("input file", workingDirectory, true);
        
        try {
            FileDecoder decoder = new FileDecoder(workingDirectory, inputFileName);
            decoder.decodeFile();
            System.out.println(
                "Decoded file " + decoder.getOutputFileName() + " placed in working directory.");
        } catch (IOException ioex) {
            System.err.println("Problem reading or writing file");
            ioex.printStackTrace();
        }
    }
    
    private static void encode() {
        String workingDirectory = UserInteraction.getDirectoryPath("working directory",
            System.getProperty("user.home"), true);
        String inputFileName = UserInteraction.getFileName("source file", workingDirectory, true);
        String targetFileName = UserInteraction.getFileName("target file", workingDirectory, true);
        String outputFileName = setFileName();
        
        try {
            FileEncoder encoder = new FileEncoder(workingDirectory, inputFileName, targetFileName,
                outputFileName);
            encoder.encodeFile();
            System.out
                .println("Output file " + outputFileName + ".png placed in working directory.");
        } catch (IOException ioex) {
            System.err.println("Problem reading or writing file");
            ioex.printStackTrace();
        }
    }
    
    private static String setFileName() {
        System.out.println("Set output file name (without extension)");
        String outputFileName = scan.nextLine();
        if (outputFileName.isEmpty()) {
            throw new IllegalArgumentException("No file name given");
        }
        System.out.println("Selected output file name: " + outputFileName + ".png");
        return outputFileName;
    }
}
