package solutions.picture.stal;

import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * An iterator over the coordinates in a BufferedImage.
 * 
 * @author Steffen Albrecht
 */
public class CoordinateIterator implements Iterator<Point> {
    
    BufferedImage image;
    int imageWidth;
    int imageHeight;
    int x = 0;
    int y = 0;
    
    /**
     * Constructor
     * 
     * @param image
     *            The image to be iterated over.
     */
    public CoordinateIterator(BufferedImage image) {
        this.image = image;
        imageWidth = image.getWidth();
        imageHeight = image.getHeight();
    }
    
    /**
     * Returns true if there are more pixels in the image.
     * 
     * @return True if there are more pixels in the image.
     */
    @Override
    public boolean hasNext() {
        return x < imageWidth && y < imageHeight;
    }
    
    /**
     * Returns the next pixel.
     * 
     * @return The next pixel.
     * @throws NoSuchElementException
     *             if there are no more pixels in the image.
     */
    @Override
    public Point next() {
        if (!hasNext()) {
            throw new NoSuchElementException();
        }
        Point position = new Point(x, y);
        if (x == imageWidth - 1) {
            x = 0;
            y++;
        } else {
            x++;
        }
        return position;
    }
}
