package solutions.picture.stal;

/**
 * Common constants for the FileEncoder and FileDecoder classes.
 *  
 * @author Steffen Albrecht
 *
 */

public final class Constants {    
    
    public static final int COLOR_COMPONENTS_PER_PIXEL = 3;
    public static final int BIT_PAIRS_PER_BYTE = 4;
    public static final int INTEGER_SIZE_IN_BYTES = 4;
    public static final int FIRST_SIX = 0xFC;
    public static final int LAST_TWO = 0x3;
    public static final int RED_FIRST_SIX = 0xFFFCFFFF;
    public static final int GREEN_FIRST_SIX = 0xFFFFFCFF;
    public static final int BLUE_FIRST_SIX = 0xFFFFFFFC;
    
    private Constants() {
    }
}
