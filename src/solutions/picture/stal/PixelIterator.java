package solutions.picture.stal;

import java.awt.image.BufferedImage;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * An iterator over the RGB color values of the pixels in a BufferedImage.
 * 
 * @author Steffen Albrecht
 */
public class PixelIterator implements Iterator<Integer> {

	BufferedImage image;
	int imageWidth;
	int imageHeight;
	int x = 0;
	int y = 0;

	/**
	 * Constructor
	 * 
	 * @param image
	 *            The image to be iterated over.
	 */
	public PixelIterator(BufferedImage image) {
		this.image = image;
		imageWidth = image.getWidth();
		imageHeight = image.getHeight();
	}

	/**
	 * Returns true if there are more pixels in the image.
	 * 
	 * @return True if there are more pixels in the image.
	 */
	@Override
	public boolean hasNext() {
		return x < imageWidth && y < imageHeight;
	}

	/**
	 * Returns the next pixel.
	 * 
	 * @return The next pixel.
	 * @throws NoSuchElementException
	 *             if there are no more pixels in the image.
	 */
	@Override
	public Integer next() {
		if (!hasNext()) {
			throw new NoSuchElementException();
		}
		int pixel = image.getRGB(x, y);
		if (x == imageWidth - 1) {
			x = 0;
			y++;
		} else {
			x++;
		}
		return pixel;
	}
}
