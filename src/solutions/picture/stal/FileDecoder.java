package solutions.picture.stal;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Iterator;
import javax.imageio.ImageIO;

/**
 * Extracts a file hidden in the least significant bits of the pixels of an image
 * file.
 * 
 * @author Steffen Albrecht
 */

public class FileDecoder {
    
    String workingDirectory;
    String outputFileName;
    Iterator<Integer> iterator;
    ByteBuffer twoBitBytes;
    ByteBuffer combinedBytes;
    int fileLength;
    final int FILE_NAME_SIZE = 50;
    
    /**
     * Constructor
     * 
     * @param workingDirectory
     *            The folder containing the source file. The hidden file, if
     *            successfully decoded, will be placed in the same directory.
     * @param inputFileName
     *            The file to be decoded within the working directory.
     * @throws IOException
     */
    public FileDecoder(String workingDirectory, String inputFileName) throws IOException {
        this.workingDirectory = workingDirectory;
        BufferedImage image = ImageIO.read(new File(workingDirectory + inputFileName));
        iterator = new PixelIterator(image);
        // Allocate two ByteBuffers for decoding.
        // Maximum number of hidden LS bit pairs = total color components in the
        // image
        int numberOfColorComponents = image.getWidth() * image.getHeight()
            * Constants.COLOR_COMPONENTS_PER_PIXEL;
        // Allocate one byte per possible LS bit pair in the buffer to write
        // them to ...
        twoBitBytes = ByteBuffer.allocate(numberOfColorComponents);
        // ... and a quarter of that number in the buffer that will hold the
        // bytes composed from these pairs.
        combinedBytes = ByteBuffer.allocate(numberOfColorComponents / Constants.BIT_PAIRS_PER_BYTE);
    }
    
    /**
     * Returns the output file name, once decoded, else null.
     * 
     * @return The output file name.
     */
    public String getOutputFileName() {
        return outputFileName;
    }
    
    /**
     * Decodes the source file and writes the hidden file to the working
     * directory.
     * 
     * @throws IOException
     */
    public void decodeFile() throws IOException {
        // First read a pixel chunk large enough to decode the file length
        read(Constants.INTEGER_SIZE_IN_BYTES * Constants.BIT_PAIRS_PER_BYTE);
        fileLength = decodeFileLength();
        // Then read enough pixels to decode the hidden file and the file name
        int estimatedTotalLength = ((fileLength + FILE_NAME_SIZE) * Constants.BIT_PAIRS_PER_BYTE);
        read(estimatedTotalLength);
        outputFileName = decodeOutputFileName();
        // Finally, read the file itself and write it to the output file.
        decodeAndWriteOutFile(workingDirectory + outputFileName);
    }
    
    // Extract the two LSBits from each color component in the image
    // into bytes up to a certain limit, then combine them into new bytes.
    private void read(int limit) {
        while (twoBitBytes.position() < limit) {
            int pixel = iterator.next();
            extractLSBits(pixel);
        }
        composeBytes();
    }
    
    // Extract the LSBits and write each pair as a byte to the
    // ByteBuffer.
    private void extractLSBits(int pixel) {
        Color color = new Color(pixel);
        twoBitBytes.put((byte) (color.getRed() & Constants.LAST_TWO));
        twoBitBytes.put((byte) (color.getGreen() & Constants.LAST_TWO));
        twoBitBytes.put((byte) (color.getBlue() & Constants.LAST_TWO));
    }
    
    // Combine the two LSBits from four consecutive bytes into a single byte
    private void composeBytes() {
        twoBitBytes.flip();
        while (twoBitBytes.remaining() >= 4) {
            combinedBytes.put((byte) (twoBitBytes.get() << 6 | twoBitBytes.get() << 4
                | twoBitBytes.get() << 2 | twoBitBytes.get()));
        }
        twoBitBytes.compact();
    }
    
    // Extract the length of the hidden file from the ByteBuffer.
    // Start position is 0.
    private int decodeFileLength() {
        combinedBytes.flip();
        int fileLength = combinedBytes.getInt();
        combinedBytes.compact();
        return fileLength;
    }
    
    // Extract the output file name from the ByteBuffer. Start position is
    // after the Integer representing the length of the hidden file has been
    // extracted. File name delimiter is a 0 byte, so stop reading when you hit
    // that.
    private String decodeOutputFileName() {
        combinedBytes.flip();
        ByteArrayOutputStream fileNameStream = new ByteArrayOutputStream();
        byte b;
        while ((b = combinedBytes.get()) != 0) {
            fileNameStream.write(b);
        }
        combinedBytes.compact();
        System.out.println(fileNameStream.toString()); // TODO delete
        return fileNameStream.toString();
    }
    
    // Extract a byte array of the same length as the hidden file length from
    // the ByteBuffer and write it to the output file. Done!
    private void decodeAndWriteOutFile(String outPath) throws IOException {
        combinedBytes.flip();
        byte[] file = new byte[fileLength];
        combinedBytes.get(file);
        try (FileOutputStream out = new FileOutputStream(outPath)) {
            out.write(file);
            out.close();
        }
    }
}
