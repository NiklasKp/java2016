package solutions.cipher.stal2;

import java.util.Random;

/**
 * An alternative solution for the Cipher exercise in the Java 2016 package not
 * using any Java 8 features.
 * 
 * @author Steffen Albrecht (stal)
 */

public class Cipher {
    
    private final String alphabet = "abcdefghijklmnopqrstuvwxyz";
    private final String key;
    private final int RANDOM_KEY_LENGTH = 100;
    private final int FORWARD = 1;
    private final int BACKWARD = -1;
    private final int ALPHABET_LENGTH = alphabet.length();
    
    public Cipher(String key) {
        this.key = key;
        if (!key.matches("^[a-z]+$")) {
            throw new IllegalArgumentException("key must contain only lowercase letters");
        }
    }
    
    public Cipher() {
        key = generateRandomKey();
    }
    
    public String getKey() {
        return key;
    }
    
    // Generate a key of random lowercase letters from the alphabet.
    private String generateRandomKey() {
        Random rand = new Random();
        StringBuffer keyBuffer = new StringBuffer();
        for (int i = 0; i < RANDOM_KEY_LENGTH; i++) {
            keyBuffer.append(alphabet.charAt(rand.nextInt(ALPHABET_LENGTH)));
        }
        return keyBuffer.toString();
    }
    
    // Encode a single character by shifting it within the alphabet.
    private char shift(char c, int offset) {
        return alphabet.charAt((alphabet.indexOf(c) + offset) % ALPHABET_LENGTH);
    }
    
    // Calculate the offset for shifting a plaintext letter from the value of
    // the key letter at the same index.
    private int getOffset(int position) {
        return alphabet.indexOf(key.charAt(position));
    }
    
    public String encode(String plaintext) {
        return encodeWithModifier(plaintext, FORWARD);
    }
    
    public String decode(String ciphertext) {
        return encodeWithModifier(ciphertext, BACKWARD);
    }
    
    // Encode or decode the input letter by shifting each letter.
    // A positive modifier causes a forward shift, a negative modifier a
    // backwards shift (for decoding).
    private String encodeWithModifier(String inText, int modifier) {
        StringBuffer outTextBuffer = new StringBuffer();
        for (int i = 0; i < inText.length(); i++) {
            outTextBuffer.append(
                shift(inText.charAt(i), ALPHABET_LENGTH + modifier * getOffset(i)));
        }
        return outTextBuffer.toString();
    }
}
