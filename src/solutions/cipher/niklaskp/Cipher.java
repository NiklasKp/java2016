package solutions.cipher.niklaskp;

import java.util.Random;

/**
 * Simple shift cipher.
 * 
 * @author Niklas Kopp
 */
public class Cipher {
  private static final char A = 'a';
  private static final char Z = 'z';
  private static final String VALID_STRING = "[a-z]+";
  private static final int ALPHABET_LENGTH = 26;
  private static final int KEY_LENGTH = 100;
  
  private String key;
  
  /**
   * Creates a new Cipher with a random key.
   */
  public Cipher() {
    key = "";
    Random r = new Random();
    for (int i = 0; i < KEY_LENGTH; i++)
      key += (char) (A + r.nextInt(ALPHABET_LENGTH));
  }
  
  /**
   * Creates a new Cipher with the assigned key.
   * 
   * @param key
   *          containing the characters 'a'-'z'
   */
  public Cipher(String key) {
    if (!key.matches(VALID_STRING)) {
      throw new IllegalArgumentException("Invalid key " + key);
    }
    this.key = key;
  }
  
  /**
   * Returns the key of this Cipher
   * 
   * @return key
   */
  public String getKey() {
    return key;
  }
  
  /**
   * Encodes the assigned plain text.
   * 
   * @param input
   *          plain text input
   * @return the encrypted output
   */
  public String encode(String input) {
    return translate(input, 1);
  }
  
  /**
   * Decodes the assigned encrypted text.
   * 
   * @param input
   *          encrypted text input
   * @return the plain text output
   */
  public String decode(String input) {
    return translate(input, -1);
  }
  
  // ______________________________________________________________
  
  // Translate method to use by both, decode and encode.
  private String translate(String input, int factor) {
    if (!input.matches(VALID_STRING)) {
      throw new IllegalArgumentException("Invalid input " + input);
    }
    String result = "";
    KeyIterator iter = new KeyIterator(getKey());
    
    for (char c : input.toCharArray()) {
      int shift = iter.next() - A;
      c += (factor * shift);
      
      // Wrap around
      if (c < A) {
        c += ALPHABET_LENGTH;
      } else if (c > Z) {
        c -= ALPHABET_LENGTH;
      }
      result += c;
    }
    return result;
  }
  
  /*
   * Iterates over a String. Starts from the beginning again if it reaches the
   * end.
   */
  private class KeyIterator {
    private String keyString;
    private int position;
    
    public KeyIterator(String key) {
      keyString = key;
      position = -1;
    }
    
    public Character next() {
      position++;
      if (position >= keyString.length())
        position = 0;
        
      return keyString.charAt(position);
    }
  }
}
