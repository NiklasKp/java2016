package solutions.cipher.stal;

import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Provisional solution for the Cipher exercise in the Java 2016 package.
 * 
 * @author Steffen Albrecht (stal)
 */

public class Cipher {
    
    private final String alphabet = "abcdefghijklmnopqrstuvwxyz";
    private final String key;
    private final int RANDOM_KEY_LENGTH = 100;
    private final int FORWARD = 1;
    private final int BACKWARD = -1;
    private final int ALPHABET_LENGTH = alphabet.length();
    
    public Cipher(String key) {
        this.key = key;
        if (!key.matches("^[a-z]+$")) {
            throw new IllegalArgumentException("key must contain only lowercase letters");
        }
    }
    
    public Cipher() {
        key = generateRandomKey();
    }
    
    public String getKey() {
        return key;
    }
    
    // Generate a key of random lowercase letters from the alphabet.
    private String generateRandomKey() {
        Random rand = new Random();
        return IntStream.range(0, RANDOM_KEY_LENGTH)
            .mapToObj(i -> String.valueOf(alphabet.charAt(rand.nextInt(ALPHABET_LENGTH))))
            .collect(Collectors.joining());
    }
    
    // Encode a single character by shifting it within the alphabet.
    private char shift(char c, int offset) {
        return alphabet.charAt((alphabet.indexOf(c) + offset) % ALPHABET_LENGTH);
    }
    
    // Calculate the offset for shifting a plaintext letter from the value of
    // the key letter at the same index.
    private int getOffset(int position) {
        return alphabet.indexOf(key.charAt(position));
    }
    
    public String encode(String plaintext) {
        return encodeWithModifier(plaintext, FORWARD);
    }
    
    public String decode(String ciphertext) {
        return encodeWithModifier(ciphertext, BACKWARD);
    }
    
    // Encode or decode the input letter by letter and join to
    // create the output.
    // A positive modifier causes a forward shift, a negative modifier a
    // backwards shift (for decoding).
    private String encodeWithModifier(String inText, int modifier) {
        return IntStream.range(0, inText.length())
            .mapToObj(i -> String.valueOf(shift(inText.charAt(i), ALPHABET_LENGTH + modifier * getOffset(i))))
            .collect(Collectors.joining());
    }
}
